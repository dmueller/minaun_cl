import os
import pyopencl as cl
import time
import numpy as np
from jinja2 import Template
import scipy.interpolate
import math

kernel_file = 'kernel_yee_o4_rect.cl'
float_type = 'double'
model = 'o2'
winding = 1
use_fft = True
gpu = 0
use_single_flux_tube = False

# flux tube positions
x0 = 0.0
y0 = 0.0

x1 = 0.5
y1 = 0.5

# Ly scaling (change this for hexagonal/square lattices)
lys = np.sqrt(3.0)


def nny(nx):
    return int(round(nx * lys))


def ci(ix, iy, nx, ny):
    ix2 = (ix + nx) % nx
    iy2 = (iy + ny) % ny
    return nx * iy2 + ix2


class Minimizer:
    def __init__(self, nx):
        if float_type == 'float':
            self.real_t = np.float32
        elif float_type == 'double':
            self.real_t = np.float64
        else:
            print("Choose either float or double for float_type!")
            exit()

        global kernel_file
        if model == 'o2':
            kernel_file = 'kernel_yee_o2_rect.cl'
        elif model == 'o4':
            kernel_file = 'kernel_yee_o4_rect.cl'
        else:
            print("Choose either o2 or o4 for model!")
            exit()

        self.nx = nx
        self.ny = nny(nx)

        # pyopencl stuff
        self.platform = cl.get_platforms()[0]
        self.device = self.platform.get_devices()[0]
        self.context = cl.Context([self.device])
        self.queue = cl.CommandQueue(self.context)

        self.x = np.zeros(4 * self.nx * self.ny, dtype=self.real_t)
        self.g = np.zeros(4 * self.nx * self.ny, dtype=self.real_t)
        self.p = np.zeros(4 * self.nx * self.ny, dtype=self.real_t)
        self.b = np.zeros(self.nx * self.ny, dtype=self.real_t)
        self.c = np.zeros(self.nx * self.ny, dtype=self.real_t)
        self.f = np.zeros(self.nx * self.ny, dtype=self.real_t)

        # copy buffers
        mf = cl.mem_flags
        self.cl_x = cl.Buffer(self.context, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=self.x)
        self.cl_g = cl.Buffer(self.context, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=self.g)
        self.cl_p = cl.Buffer(self.context, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=self.p)
        self.cl_b = cl.Buffer(self.context, mf.READ_WRITE | mf.COPY_HOST_PTR, hostbuf=self.b)
        self.cl_f = cl.Buffer(self.context, mf.WRITE_ONLY | mf.COPY_HOST_PTR, hostbuf=self.f)
        self.cl_c = cl.Buffer(self.context, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=self.c)

    def minimize(self, m, mu, la, q, h, pa, pb, l, epsilon, iters_per_step, total_steps, xa=None):
        nx = self.nx
        ny = self.ny

        self.ax = l / self.nx
        self.ay = (l * lys) / self.ny

        fac = self.ax
        iax = 1.0
        iay = self.ax / self.ay

        folder = os.path.dirname(os.path.realpath(__file__))

        with open(folder + "/kernels/" + kernel_file, 'r') as f:
            kernel_src = f.read()

        kernel_template = Template(kernel_src)
        kernel_src = kernel_template.render(m1=m[0] * fac, m2=m[1] * fac, mu1=mu[0] * fac, mu2=mu[1] * fac,
                                            l1=la[0], l2=la[1], q=q, h=h,
                                            pa1=pa[0], pa2=pa[1], pa3=pa[2], pa4=pa[3],
                                            pb1=pb[0], pb2=pb[1], pb3=pb[2], pb4=pb[3],
                                            pi=np.pi, nx=nx, ny=ny,
                                            iax=iax, iay=iay, float_type=float_type)
        kernel_src = kernel_src.encode('ascii', 'ignore')
        kernel = cl.Program(self.context, kernel_src).build()

        # data and buffers
        if xa is None:
            self.x[:] = 0
        else:
            self.x = np.array(xa, dtype=self.real_t)

        # initial condition for r1 and r2
        rho_sc = np.sqrt((mu[0] ** 2 - m[0] ** 2) / la[0]) * fac
        rho_sf = np.sqrt((mu[1] ** 2 - m[1] ** 2) / la[1]) * fac

        rho_1 = np.sqrt(la[1] * (la[0] * rho_sc ** 2 + h * rho_sf ** 2) / (la[0] * la[1] - h ** 2))
        rho_2 = np.sqrt(la[0] * (la[1] * rho_sf ** 2 + h * rho_sc ** 2) / (la[0] * la[1] - h ** 2))

        if xa is None:
            self.x[0::4] = rho_1
            self.x[1::4] = rho_2

        # place two flux tubes at positions (x0,y0) and (x1,y1)
        self.c[ci(int(round(x0 * nx)), int(round(y0 * ny)), nx, ny)] += 2 * np.pi / (self.ax * self.ay) * fac ** 2 * winding
        if not use_single_flux_tube:
            self.c[ci(int(round(x1 * nx)), int(round(y1 * ny)), nx, ny)] += 2 * np.pi / (self.ax * self.ay) * fac ** 2 * winding

        # fft initial conditions for Cx and Cy.
        # based on assuming a homogeneous magnetic field.
        if use_fft:
            prnt("Creating Cx, Cy initial conditions from FFT.")
            # fft initial condition for Cx and Cy
            ix = np.arange(0, nx)
            iy = np.arange(0, ny)
            xx, yy = np.meshgrid(ix, iy)
            k = 2.0 * iax ** 2 * (np.cos(2 * xx * np.pi / nx) - 1) + 2.0 * iay ** 2 * (np.cos(2* yy * np.pi / ny) - 1) + 1E-20
            k = k.T ** (-1)
            k[0, 0] = 0.0
            c_fft = np.fft.fft2(np.reshape(self.c, (ny, nx)).T)
            u = np.real(np.fft.ifft2(c_fft * k))
            cx = - iay * (np.roll(u, +1, 1) - u)
            cy = + iax * (np.roll(u, +1, 0) - u)
            self.x[2::4] = cx.T.flatten()
            self.x[3::4] = cy.T.flatten()
            prnt("Done.")

        # copy initial conditions to gpu
        cl.enqueue_copy(self.queue, self.cl_x, self.x).wait()
        cl.enqueue_copy(self.queue, self.cl_c, self.c).wait()

        # run
        free_energy = 0.0
        free_energy_0 = 100000000000.0
        g_flux_0 = 100000000000.0
        h_ext = 0
        b_avg = 0

        for r in xrange(total_steps):
            t0 = time.time()
            ls = None
            for i in xrange(iters_per_step):
                kernel.magnetic(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c)
                kernel.gradient(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c)
                kernel.evolve(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c)

            kernel.magnetic(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c).wait()
            kernel.free_energy(self.queue, (nx * ny,), ls, self.cl_x, self.cl_b, self.cl_f).wait()
            cl.enqueue_copy(self.queue, self.g, self.cl_g)
            cl.enqueue_copy(self.queue, self.f, self.cl_f)
            cl.enqueue_copy(self.queue, self.x, self.cl_x)

            self.queue.finish()
            free_energy = np.sum(self.f) / (nx * ny * fac ** 4)

            kernel.free_energy_cheap(self.queue, (nx * ny,), ls, self.cl_x, self.cl_b, self.cl_f).wait()
            cl.enqueue_copy(self.queue, self.f, self.cl_f)
            free_energy_cheap = np.sum(self.f) / (nx * ny * fac ** 4)

            # This is the relative change of the free energy density per iteration
            df = np.abs((free_energy - free_energy_0) / free_energy_0 / iters_per_step)

            kernel.magnetic(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c)
            cl.enqueue_copy(self.queue, self.x, self.cl_x).wait()
            cl.enqueue_copy(self.queue, self.b, self.cl_b).wait()
            self.queue.finish()

            # external field calculation
            ls = None
            kernel.f_kin(self.queue, (nx * ny,), ls, self.cl_x, self.cl_b, self.cl_f).wait()
            cl.enqueue_copy(self.queue, self.f, self.cl_f)
            f_kin = np.sum(self.f) / (nx * ny * fac ** 4)

            kernel.f_mag(self.queue, (nx * ny,), ls, self.cl_x, self.cl_b, self.cl_f).wait()
            cl.enqueue_copy(self.queue, self.f, self.cl_f)
            f_mag = np.sum(self.f) / (nx * ny * fac ** 4)

            b_avg = max(np.mean(self.b) / (self.ax ** 2 * q), 10E-14)
            h_ext = 4 * np.pi * (f_kin + 2 * f_mag) / b_avg

            g_flux = free_energy - b_avg * h_ext / (4 * np.pi)

            # This is the relative change of the Gibbs free energy density per iteration
            dg_flux = np.abs((g_flux - g_flux_0) / g_flux_0 / iters_per_step)

            dt = time.time() - t0

            otp = "{:04d}: |dF| = {:.2E}, |dG| = {:.2E}, F/V = {:f}, G/V = {:f}, H_ext = {:f}, n={:d}/s @ {:d}x{:d}"
            prnt(otp.format(r, df, dg_flux, free_energy, g_flux,
                             h_ext, int(iters_per_step / dt), nx, ny))

            # stopping criterion based on F and G (whichever is larger)
            dg = np.max([dg_flux, df])
            if dg < epsilon:
                prnt("F/V = {}.".format(free_energy))
                break
            elif math.isnan(dg):
                print("Error: encountered NaN. Increase grid size or reduce pa and pb for better stability.")
                exit()

            free_energy_0 = free_energy
            g_flux_0 = g_flux

        ls = None
        kernel.magnetic(self.queue, (nx * ny,), ls, self.cl_x, self.cl_g, self.cl_p, self.cl_b, self.cl_c)
        cl.enqueue_copy(self.queue, self.x, self.cl_x).wait()
        cl.enqueue_copy(self.queue, self.b, self.cl_b).wait()
        self.queue.finish()

        prnt("H_ext = {}.".format(h_ext))
        prnt("B_avg = {}.".format(b_avg))

        self.queue.finish()

        return self.x, self.b, free_energy, h_ext, b_avg

    def ren_x(self, ao, an):
        self.x *= an / ao


class MultigridMinimizer:
    def __init__(self, nx_r):
        self.nx_r = nx_r
        self.mn = []
        for nx in nx_r:
            self.mn.append(Minimizer(nx))

    def minimize(self, m, mu, la, q, h, pa, pb, l, epsilon_r, iters_per_step, total_steps, xa1=None):
        # check if given a list of epsilons
        # if not a list, make it one
        if type(epsilon_r) is not list:
            epsilon_r = [epsilon_r] * len(self.mn)

        results = []
        prnt("Grid no. 1")
        x, b, gibbs, h_ext, b_avg = self.mn[0].minimize(m, mu, la, q, h, pa, pb, l,  epsilon_r[0], iters_per_step, total_steps, xa=xa1)
        results.append(gibbs)
        results.append(h_ext)
        results.append(b_avg)
        for i in range(1, len(self.mn)):
            prnt("Interpolation no. {}".format(i))
            xn1 = rg_x(self.mn[i-1].x, self.nx_r[i-1], self.nx_r[i], use_fft)
            prnt("Grid no. {}".format(i+1))
            x, b, gibbs, h_ext, b_avg = self.mn[i].minimize(m, mu, la, q, h, pa, pb, l, epsilon_r[i], iters_per_step, total_steps, xa=xn1)
            results.append(gibbs)
            results.append(h_ext)
            results.append(b_avg)

        return results, x, b

    def ren_x(self, ao, an):
        self.mn[0].ren_x(ao, an)


def prnt(a):
    print(a)
    pass


"""
    Old grid module
"""


def rg(data, nx, new_nx):
    xo, yo = np.linspace(0, 1, nx), np.linspace(0, 1, nny(nx))
    Xo, Yo = np.meshgrid(xo, yo)
    xn, yn = np.linspace(0, 1, new_nx), np.linspace(0, 1, nny(new_nx))
    Xn, Yn = np.meshgrid(xn, yn)

    new_data = scipy.interpolate.griddata((Xo.flatten(), Yo.flatten()), data, (Xn.flatten(), Yn.flatten()),
                                          method='cubic')
    return new_data


def rg_x(x, nx, nnx, use_fft=False):
    r1, r2 = rg(x[0::4], nx, nnx), rg(x[1::4], nx, nnx)
    if not use_fft:
        cx, cy = rg(x[2::4], nx, nnx), rg(x[3::4], nx, nnx)
    xn = np.zeros(4 * nnx * nny(nnx), dtype=x.dtype)
    xn[0::4] = r1 * nx / nnx
    xn[1::4] = r2 * nx / nnx
    if not use_fft:
        xn[2::4] = cx * nx / nnx
        xn[3::4] = cy * nx / nnx
    return xn
