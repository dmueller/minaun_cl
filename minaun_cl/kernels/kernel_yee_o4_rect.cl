#pragma OPENCL EXTENSION cl_khr_fp64: enable

// Kernel with O(a^4) Yee

#define USE_CHEAP           0

// Define all parameters here (replaced in python)
#define M2      {{m2}}
#define M1      {{m1}}
#define MU1     {{mu1}}
#define MU2     {{mu2}}
#define H       {{h}}
#define L1      {{l1}}
#define L2      {{l2}}
#define Q       {{q}}

#define PA1  {{pa1}}
#define PA2  {{pa2}}
#define PA3  {{pa3}}
#define PA4  {{pa4}}

#define PB1  {{pb1}}
#define PB2  {{pb2}}
#define PB3  {{pb3}}
#define PB4  {{pb4}}

#define PI      {{pi}}
#define NX      {{nx}}
#define NY      {{ny}}
#define GF      1.0/(4.0*{{pi}}*{{q}}*{{q}})
#define IAX     {{iax}}
#define IAY     {{iay}}
#define IAX2    {{iax}} * {{iax}}
#define IAY2    {{iay}} * {{iay}}

// finite difference coefficients

// first staggered central  difference
#define F1  +9.0/8.0
#define F2  -1.0/24.0

// second staggered central difference
#define C0  -365.0/144.0
#define C1  +87.0/64.0
#define C2  -3.0/32.0
#define C3  +1.0/576.0

// staggered average coefficients
#define A1  +9.0/16.0
#define A2  -1.0/16.0

// other stuff
#define fi(i, o)  4*i + o
#define F    __global real_t*
#define p2(x)     x * x
#define p3(x)     x * x * x
#define p4(x)     x * x * x * x

typedef {{float_type}} real_t;

// bad shift
inline int ci(int ix, int iy) {
  ix = (ix + NX) % NX;
  iy = (iy + NY) % NY;
  return NX * iy + ix;
}

// Computes the gradient g(x)
__kernel void gradient(F x, F g, F p, F b, F c)
{
  // nearest neighbours
  int i = get_global_id(0);
  int ix = i % NX;
  int iy = i / NX;

  int i_r1 = ci(ix+1, iy);
  int i_r2 = ci(ix+2, iy);
  int i_r3 = ci(ix+3, iy);

  int i_l1 = ci(ix-1, iy);
  int i_l2 = ci(ix-2, iy);
  int i_l3 = ci(ix-3, iy);

  int i_u1 = ci(ix, iy+1);
  int i_u2 = ci(ix, iy+2);
  int i_u3 = ci(ix, iy+3);

  int i_d1 = ci(ix, iy-1);
  int i_d2 = ci(ix, iy-2);
  int i_d3 = ci(ix, iy-3);

  // r1 gradient
  g[fi(i, 0)] = - (IAX2 + IAY2) * C0 * x[fi(i, 0)] - \
    IAX2 * (C3 * x[fi(i_r3, 0)] + C2 * x[fi(i_r2, 0)] + C1 * x[fi(i_r1, 0)]) - \
    IAX2 * (C3 * x[fi(i_l3, 0)] + C2 * x[fi(i_l2, 0)] + C1 * x[fi(i_l1, 0)]) - \
    IAY2 * (C3 * x[fi(i_u3, 0)] + C2 * x[fi(i_u2, 0)] + C1 * x[fi(i_u1, 0)]) - \
    IAY2 * (C3 * x[fi(i_d3, 0)] + C2 * x[fi(i_d2, 0)] + C1 * x[fi(i_d1, 0)]) + \
    ((p2(M1) - p2(MU1)) + L1 * p2(x[fi(i, 0)])) * x[fi(i, 0)] + \
    x[fi(i, 0)] * (A2 * p2(x[fi(i_r1, 2)]) + A1 * p2(x[fi(i, 2)]) + A1 * p2(x[fi(i_l1, 2)]) + A2 * p2(x[fi(i_l2, 2)])) + \
    x[fi(i, 0)] * (A2 * p2(x[fi(i_u1, 3)]) + A1 * p2(x[fi(i, 3)]) + A1 * p2(x[fi(i_d1, 3)]) + A2 * p2(x[fi(i_d2, 3)])) - \
    H * x[fi(i, 0)] * p2(x[fi(i, 1)]);

  // r2 gradient
  g[fi(i, 1)] = - (IAX2 + IAY2) * C0 * x[fi(i, 1)] - \
    IAX2 * (C3 * x[fi(i_r3, 1)] + C2 * x[fi(i_r2, 1)] + C1 * x[fi(i_r1, 1)]) - \
    IAX2 * (C3 * x[fi(i_l3, 1)] + C2 * x[fi(i_l2, 1)] + C1 * x[fi(i_l1, 1)]) - \
    IAY2 * (C3 * x[fi(i_u3, 1)] + C2 * x[fi(i_u2, 1)] + C1 * x[fi(i_u1, 1)]) - \
    IAY2 * (C3 * x[fi(i_d3, 1)] + C2 * x[fi(i_d2, 1)] + C1 * x[fi(i_d1, 1)]) + \
    ((p2(M2) - p2(MU2)) + L2 * p2(x[fi(i, 1)])) * x[fi(i, 1)] - \
    H * x[fi(i, 1)] * p2(x[fi(i, 0)]);

  // cx gradient
  g[fi(i, 2)] =  + (A2 * p2(x[fi(i_r2, 0)]) + A1 * p2(x[fi(i_r1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_l1, 0)])) * x[fi(i, 2)] + \
    IAY * (+ F2 * b[i_u1] + F1 * b[i] - F1 * b[i_d1] - F2 * b[i_d2]) * GF;

  // cy gradient
  g[fi(i, 3)] =  + (A2 * p2(x[fi(i_u2, 0)]) + A1 * p2(x[fi(i_u1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_d1, 0)])) * x[fi(i, 3)] - \
    IAX * (+ F2 * b[i_r1] + F1 * b[i] - F1 * b[i_l1] - F2 * b[i_l2]) * GF;
}

// Compute the magnetic field B
__kernel void magnetic(F x, F g, F p, F b, F c)
{
  int i = get_global_id(0);
  int ix = i % NX;
  int iy = i / NX;

  int i_r1 = ci(ix+1, iy);
  int i_r2 = ci(ix+2, iy);

  int i_l1 = ci(ix-1, iy);
  int i_l2 = ci(ix-2, iy);

  int i_u1 = ci(ix, iy+1);
  int i_u2 = ci(ix, iy+2);

  int i_d1 = ci(ix, iy-1);
  int i_d2 = ci(ix, iy-2);

  // magnetic field
  b[i] = c[i] + \
    IAX * (+ F2 * x[fi(i_r2, 3)] + F1 * x[fi(i_r1, 3)] - F1 * x[fi(i, 3)] - F2 * x[fi(i_l1, 3)]) - \
    IAY * (+ F2 * x[fi(i_u2, 2)] + F1 * x[fi(i_u1, 2)] - F1 * x[fi(i, 2)] - F2 * x[fi(i_d1, 2)]);
}


// Applies accelerated gradient (heavy-ball method)
__kernel void evolve(F x, F g, F p, F b, F c)
{
  int i = get_global_id(0);
  p[fi(i, 0)] = PB1 * p[fi(i, 0)] - g[fi(i, 0)];
  p[fi(i, 1)] = PB2 * p[fi(i, 1)] - g[fi(i, 1)];
  p[fi(i, 2)] = PB3 * p[fi(i, 2)] - g[fi(i, 2)];
  p[fi(i, 3)] = PB4 * p[fi(i, 3)] - g[fi(i, 3)];

  x[fi(i, 0)] += PA1 * p[fi(i, 0)];
  x[fi(i, 1)] += PA2 * p[fi(i, 1)];
  x[fi(i, 2)] += PA3 * p[fi(i, 2)];
  x[fi(i, 3)] += PA4 * p[fi(i, 3)];
}

// Compute the free energy (which is being minimized)
__kernel void free_energy(F x, F b, F f)
{
  // nearest neighbours
  int i = get_global_id(0);
  int ix = i % NX;
  int iy = i / NX;

  int i_r1 = ci(ix+1, iy);
  int i_r2 = ci(ix+2, iy);
  int i_r3 = ci(ix+3, iy);

  int i_l1 = ci(ix-1, iy);
  int i_l2 = ci(ix-2, iy);
  int i_l3 = ci(ix-3, iy);

  int i_u1 = ci(ix, iy+1);
  int i_u2 = ci(ix, iy+2);
  int i_u3 = ci(ix, iy+3);

  int i_d1 = ci(ix, iy-1);
  int i_d2 = ci(ix, iy-2);
  int i_d3 = ci(ix, iy-3);

  real_t func = 0.0;

  func += 0.50 * (- (IAX2 + IAY2) * C0 * x[fi(i, 0)] - \
    IAX2 * (C3 * x[fi(i_r3, 0)] + C2 * x[fi(i_r2, 0)] + C1 * x[fi(i_r1, 0)]) - \
    IAX2 * (C3 * x[fi(i_l3, 0)] + C2 * x[fi(i_l2, 0)] + C1 * x[fi(i_l1, 0)]) - \
    IAY2 * (C3 * x[fi(i_u3, 0)] + C2 * x[fi(i_u2, 0)] + C1 * x[fi(i_u1, 0)]) - \
    IAY2 * (C3 * x[fi(i_d3, 0)] + C2 * x[fi(i_d2, 0)] + C1 * x[fi(i_d1, 0)])) * x[fi(i, 0)];

  func += 0.50 * (- (IAX2 + IAY2) * C0 * x[fi(i, 1)] - \
    IAX2 * (C3 * x[fi(i_r3, 1)] + C2 * x[fi(i_r2, 1)] + C1 * x[fi(i_r1, 1)]) - \
    IAX2 * (C3 * x[fi(i_l3, 1)] + C2 * x[fi(i_l2, 1)] + C1 * x[fi(i_l1, 1)]) - \
    IAY2 * (C3 * x[fi(i_u3, 1)] + C2 * x[fi(i_u2, 1)] + C1 * x[fi(i_u1, 1)]) - \
    IAY2 * (C3 * x[fi(i_d3, 1)] + C2 * x[fi(i_d2, 1)] + C1 * x[fi(i_d1, 1)])) * x[fi(i, 1)];

  func += 0.50 * (p2(M1) - p2(MU1)) * p2(x[fi(i, 0)]);
  func += 0.25 * L1 * p4(x[fi(i, 0)]);

  func += 0.50 * (p2(M2) - p2(MU2)) * p2(x[fi(i, 1)]);
  func += 0.25 * L2 * p4(x[fi(i, 1)]);

  func += -0.50 * H * p2(x[fi(i, 0)]) * p2(x[fi(i, 1)]);

  func += 0.50 * (A2 * p2(x[fi(i_r2, 0)]) + A1 * p2(x[fi(i_r1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_l1, 0)])) * p2(x[fi(i, 2)]);
  func += 0.50 * (A2 * p2(x[fi(i_u2, 0)]) + A1 * p2(x[fi(i_u1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_d1, 0)])) * p2(x[fi(i, 3)]);

  func += 0.50 * p2(b[i]) * GF;

  f[i] = func;
}

// Compute the kinetic part of the free energy
__kernel void f_kin(F x, F b, F f)
{
  // nearest neighbours
  int i = get_global_id(0);
  real_t func = 0.0;

  #if USE_CHEAP == 0

  int ix = i % NX;
  int iy = i / NX;

  int i_r1 = ci(ix+1, iy);
  int i_r2 = ci(ix+2, iy);
  int i_r3 = ci(ix+3, iy);

  int i_l1 = ci(ix-1, iy);
  int i_l2 = ci(ix-2, iy);
  int i_l3 = ci(ix-3, iy);

  int i_u1 = ci(ix, iy+1);
  int i_u2 = ci(ix, iy+2);
  int i_u3 = ci(ix, iy+3);

  int i_d1 = ci(ix, iy-1);
  int i_d2 = ci(ix, iy-2);
  int i_d3 = ci(ix, iy-3);


  func += 0.50 * (- (IAX2 + IAY2) * C0 * x[fi(i, 0)] - \
    IAX2 * (C3 * x[fi(i_r3, 0)] + C2 * x[fi(i_r2, 0)] + C1 * x[fi(i_r1, 0)]) - \
    IAX2 * (C3 * x[fi(i_l3, 0)] + C2 * x[fi(i_l2, 0)] + C1 * x[fi(i_l1, 0)]) - \
    IAY2 * (C3 * x[fi(i_u3, 0)] + C2 * x[fi(i_u2, 0)] + C1 * x[fi(i_u1, 0)]) - \
    IAY2 * (C3 * x[fi(i_d3, 0)] + C2 * x[fi(i_d2, 0)] + C1 * x[fi(i_d1, 0)])) * x[fi(i, 0)];

  func += 0.50 * (- (IAX2 + IAY2) * C0 * x[fi(i, 1)] - \
    IAX2 * (C3 * x[fi(i_r3, 1)] + C2 * x[fi(i_r2, 1)] + C1 * x[fi(i_r1, 1)]) - \
    IAX2 * (C3 * x[fi(i_l3, 1)] + C2 * x[fi(i_l2, 1)] + C1 * x[fi(i_l1, 1)]) - \
    IAY2 * (C3 * x[fi(i_u3, 1)] + C2 * x[fi(i_u2, 1)] + C1 * x[fi(i_u1, 1)]) - \
    IAY2 * (C3 * x[fi(i_d3, 1)] + C2 * x[fi(i_d2, 1)] + C1 * x[fi(i_d1, 1)])) * x[fi(i, 1)];

  func += 0.50 * (A2 * p2(x[fi(i_r2, 0)]) + A1 * p2(x[fi(i_r1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_l1, 0)])) * p2(x[fi(i, 2)]);
  func += 0.50 * (A2 * p2(x[fi(i_u2, 0)]) + A1 * p2(x[fi(i_u1, 0)]) + A1 * p2(x[fi(i, 0)]) + A2 * p2(x[fi(i_d1, 0)])) * p2(x[fi(i, 3)]);

  #else

  func -= 0.50 * (p2(M1) - p2(MU1)) * p2(x[fi(i, 0)]);
  func -= 0.50 * L1 * p4(x[fi(i, 0)]);
  func -= 0.50 * (p2(M2) - p2(MU2)) * p2(x[fi(i, 1)]);
  func -= 0.50 * L2 * p4(x[fi(i, 1)]);
  func += H * p2(x[fi(i, 0)]) * p2(x[fi(i, 1)]);

  #endif

  f[i] = func;
}


// Compute the magnetic part of the free energy
__kernel void f_mag(F x, F b, F f)
{
  int i = get_global_id(0);
  real_t func = 0.0;
  func += 0.50 * p2(b[i]) * GF;
  f[i] = func;
}

// Compute the free energy (which is being minimized) faster. This is only correct if we are already at the minimum.
__kernel void free_energy_cheap(F x, F b, F f)
{
  // nearest neighbours
  int i = get_global_id(0);

  real_t func = 0.0;

  func -= 0.25 * L1 * p4(x[fi(i, 0)]);
  func -= 0.25 * L2 * p4(x[fi(i, 1)]);
  func += 0.50 * H * p2(x[fi(i, 0)]) * p2(x[fi(i, 1)]);

  func += 0.50 * p2(b[i]) * GF;

  f[i] = func;
}