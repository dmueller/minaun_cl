# minaun_cl

## Setup instructions

This code is written in Python 2.7.
We assume you have an OpenCL 1.2 compatible GPU installed in your system including drivers needed for running OpenCL programs.

### Using Anaconda

Create a new environment for `minaun_cl` and install the necessary packages using `conda`.
Simply run:

```
conda create -n minaun python=2.7
conda activate minaun
conda install numpy scipy jinja2
conda install -c conda-forge ocl-icd-system pyopencl
```

If you want to use Jupyter notebooks also run:
```
conda install jupyter matplotlib
```

To start the Jupyter notebook server:
```
jupyter notebook
```

### Using pip

For a basic installation (e.g. for using command line scripts) you need these packages:
```
pip install numpy scipy pyopencl jinja2 
```

If you want to use the Jupyter notebook provided in the `notebook` folder, you will also need

```
pip install jupyter matplotlib
```

To start the Jupyter notebook server:
```
jupyter notebook
```
