import os
import time
import numpy as np
import matplotlib.pyplot as plt
import minaun_cl.core as core
import json

"""
    Let's create a library of solutions! Woo!
"""

alphas = [0.280, 0.290, 0.300, 0.310, 0.320, 0.330, 0.340, 0.350, 0.360, 0.370, 0.380]
B = list(np.linspace(0.1, 4.5, 200))
folder = "/home/dmueller/PycharmProjects/minaun_cl/jsons/Hc1/"

if not os.path.isdir(folder):
    os.makedirs(folder)

for al in alphas:
    p = {
        # output folder
        'out': "/home/dmueller/PycharmProjects/minaun_cl/output/alpha_{:f}_512_Hc1/".format(al),

        # physical parameters
        'm': [1.0, 1.0],
        'mu': [1.5, 1.8],
        'la_s': [0.25, 1.20],
        'la_e': [0.10, 0.10],
        'alpha': al,
        'q': 0.17,
        'h': -0.1,
        'B': B,
        'winding': 1,

        # numerical parameters
        'float_type': 'double',
        'iters_per_step': 1000,
        'total_steps': 500,
        'nx': [64, 128],
        'pa': [0.19, 0.19, 0.09, 0.09],
        'pb': [0.40, 0.40, 0.60, 0.60],
        'epsilon': 1E-10,
        'delta': 2E-7,
    }

    with open(folder + "alpha_{:f}_512_Hc1.json".format(al), "w") as text_file:
        p_txt = json.dumps(p, indent=4, separators=(',', ': '))
        text_file.write(p_txt)
        text_file.close()
