import os
import time
import numpy as np
import matplotlib.pyplot as plt
import minaun_cl.core as core
import json
import glob


"""
    Let's read in some jsons. Woo!
"""

files = glob.glob("/home/dmueller/PycharmProjects/minaun_cl/jsons/Hc1/*.json")

params = []
for f in files:
    data = open(f).read()
    p = json.loads(data)
    params.append(p)

for p in params:
    core.winding = p['winding']
    core.float_type = p['float_type']
    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])

    Phi = 4 * np.pi / p['q']
    lr = np.sqrt(Phi / np.array(p['B']) / np.sqrt(3))


    # make dir, write params file
    if not os.path.isdir(p['out']):
        os.mkdir(p['out'])

    ny = [core.nny(n) for n in p['nx']]

    with open(p['out'] + "params.json", "w") as text_file:
        p_txt = json.dumps(p, indent=4, separators=(',', ': '))
        text_file.write(p_txt)
        text_file.close()

    mg = core.MultigridMinimizer(p['nx'])
    l0 = lr[0]
    results = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'], l0, p['epsilon'], p['delta'],
                          p['iters_per_step'], p['total_steps'], run=-1)

    gibbs_t = []

    output = []
    for i, l in enumerate(lr):
        t0 = time.time()
        print("L = {}.".format(l))
        l1 = l
        mg.ren_x(ao=l0, an=l1)
        results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                                    l1, p['epsilon'], p['delta'], p['iters_per_step'], p['total_steps'],
                                    i, xa1=mg.mn[0].x)
        gibbs_t.append(results)

        out_file = 'data_{:05d}.dat'.format(i)

        output.append(np.concatenate(([l], results)))
        np.savetxt(p['out'] + 'results.txt', output)

        x.tofile(p['out'] + out_file)
        print("Took me {:d}s.".format(int(time.time() - t0)))


