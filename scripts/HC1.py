"""
    A script to compute the transition from the coexistence
    phase to the flux tube phase.
"""
import os
import time
import numpy as np
# import matplotlib.pyplot as plt
import minaun_cl.core as core
import json
import glob

# params
p = {
    # physical parameters
    'm': [1.0, 1.0],
    'mu': [1.5, 1.8],
    'la_s': [0.25, 1.20],
    'la_e': [0.10, 0.10],
    'alpha': 0.350,
    'q': 0.17,
    'h': -0.1,
    'winding': 1,

    # numerical parameters
    'float_type': 'double',
    'model': 'o2',
    'iters_per_step': 1000,
    'total_steps': 500,
    'nx': [32, 64, 128],
    'pa': [0.19, 0.19, 0.09, 0.09],
    'pb': [0.20, 0.20, 0.20, 0.20],
    'epsilon': 1E-7,
    'delta': 1E-7,
    'bisec_acc': 1E-6,
}

# initial interval
B0 = 0.2
B1 = 6.0


def compute_g_ratio(p, b):
    core.winding = p['winding']
    core.float_type = p['float_type']
    core.model = p['model']
    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])
    phi = 4 * np.pi / p['q']
    lr = np.sqrt(phi / b / np.sqrt(3))
    ny = [core.nny(n) for n in p['nx']]

    t0 = time.time()
    mg = core.MultigridMinimizer(p['nx'])
    results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                                lr, p['epsilon'], p['delta'], p['iters_per_step'], p['total_steps'],
                                0, xa1=None)

    # print("Took me {:d}s.".format(int(time.time() - t0)))

    f_flux = results[-3]
    h_ext = results[-2]
    b_avg = results[-1]
    g_flux = f_flux - h_ext * b_avg / (4 * np.pi)

    r01 = np.sqrt((p['mu'][0] ** 2 - p['m'][0] ** 2) / p['la'][0])
    r02 = np.sqrt((p['mu'][1] ** 2 - p['m'][1] ** 2) / p['la'][1])
    la1 = p['la'][0]
    la2 = p['la'][1]
    h = p['h']
    g_coe = - la1 * la2 * (la1 * r01 ** 4 + la2 * r02 ** 4 + 2 * h * r01 ** 2 * r02 ** 2) / (4 * la1 * la2 - 4 * h ** 2)

    return g_flux / g_coe, h_ext, x, b

# shutup
def _print(a):
    pass
core.prnt = _print

# bisection
g0, H0, x, b = compute_g_ratio(p, B0)
g1, H1, x, b = compute_g_ratio(p, B1)
print(g0, g1)

if (g0 - 1.0) * (g1 - 1.0) > 0.0:
    print("This bisection interval does not make any sense.")
    exit()
else:
    print("This interval seems okay.")

for i in range(100):
    BN = np.sqrt(B0 * B1)
    gn, h_ext, x, b = compute_g_ratio(p, BN)

    if gn < 1.0:
        B0 = BN
        H0 = h_ext
        g0 = gn
    else:
        B1 = BN
        H1 = h_ext
        g1 = gn

    print(g0, g1)
    print(B0, B1)
    print(H0, H1)
    print(i)

    if i >= 2:
        acc = abs((h_ext0 - h_ext) / h_ext0)
        if acc <= p['bisec_acc']:
            break

    h_ext0 = h_ext

