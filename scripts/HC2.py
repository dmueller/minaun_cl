"""
    A script to compute the transition from the flux tube
    phase to the normal phase.
"""
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import minaun_cl.core as core
import json
import glob

# params
p = {
    # physical parameters
    'm': [1.0, 1.0],
    'mu': [1.5, 1.8],
    'la_s': [0.25, 1.20],
    'la_e': [0.10, 0.10],
    'alpha': 0.15,
    'q': 0.17,
    'h': -0.2,
    'winding': 1,

    # numerical parameters
    'float_type': 'double',
    'model': 'o4',
    'iters_per_step': 5000,
    'total_steps': 500,
    'nx': [32, 64, 128],
    'pa': [0.24, 0.24, 0.09, 0.09],
    'pb': [0.97, 0.97, 0.97, 0.97],
    'epsilon': 1E-12,
    'delta': 1E-12,
}

# initial interval
B0 = 4.7
B1 = 5.0


def compute_g_ratio(p, b):
    core.winding = p['winding']
    core.float_type = p['float_type']
    core.model = p['model']
    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])
    phi = 4 * np.pi / p['q']
    lr = np.sqrt(phi / b / np.sqrt(3))
    ny = [core.nny(n) for n in p['nx']]

    t0 = time.time()
    mg = core.MultigridMinimizer(p['nx'])
    results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                                lr, 0, p['delta'], p['iters_per_step'], p['total_steps'],
                                0, xa1=None)

    print("Took me {:d}s.".format(int(time.time() - t0)))

    f_flux = results[-3]
    h_ext = results[-2]
    b_avg = results[-1]
    g_flux = f_flux - h_ext * b_avg / (4 * np.pi)

    r01 = np.sqrt((p['mu'][0] ** 2 - p['m'][0] ** 2) / p['la'][0])
    r02 = np.sqrt((p['mu'][1] ** 2 - p['m'][1] ** 2) / p['la'][1])
    la1 = p['la'][0]
    la2 = p['la'][1]
    h = p['h']
    g_nor = - 0.25 * la2 * r02 ** 4 - h_ext ** 2 / (8 * np.pi)

    return round(g_flux / g_nor, 10), round(h_ext, 10)

# shutup
def _print(a):
    pass
#core.prnt = _print


# bisection
g0, h_ext0 = compute_g_ratio(p, B0)
g1, h_ext1 = compute_g_ratio(p, B1)
print("g0, g1, B0, B1, H0, H1")
print(g0, g1, B0, B1, h_ext0, h_ext1)

for i in range(100):
    BN = np.sqrt(B0 * B1)
    #BN = (B0 - B1 + B1 * g0 - B0 * g1) / (g0 - g1)

    gn, h_ext = compute_g_ratio(p, BN)

    if gn > 1.0:
        B0 = BN
        h_ext0 = h_ext
        g0 = gn
    else:
        B1 = BN
        h_ext1 = h_ext
        g1 = gn

    print("g0, g1, B0, B1, H0, H1")
    print(g0, g1, B0, B1, h_ext0, h_ext1)
