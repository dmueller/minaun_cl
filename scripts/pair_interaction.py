"""
    A script for studying the pair interaction of two flux tubes.
    Profiles are exported as well.
"""

import os
import time
import numpy as np
import minaun_cl.core as core


use_profile_output = False
use_display = False
output = "/home/dmueller/fluxtubes/"
result_file = "single_512_6.txt"

# Set simulation area to square
core.lys = 1.0
core.use_single_flux_tube = True

r_list = np.linspace(0.05, 0.25, 25+1)
r_list = [0.0]

# Average magnetic field
B = 1.0

# parameter object
p = {
    # physical parameters
    'm': [1.0, 1.0],
    'mu': [1.5, 1.8],
    'la_s': [0.25, 1.20],
    'la_e': [0.10, 0.10],
    'alpha': 0.320,
    'q': 0.17,
    'h': -0.1,
    'winding': 1,

    # numerical parameters
    'float_type': 'double',
    'model': 'o4',
    'iters_per_step': 1000,
    'total_steps': 500,
    'nx': [64, 128, 256, 512, 1024],
    'pa': [0.33, 0.33, 0.13, 0.13],
    'pb': [0.98, 0.98, 0.98, 0.98],
    #'pa': [0.1, 0.1, 0.05, 0.05],
    #'pb': [0.7, 0.7, 0.7, 0.7],
    'epsilon': 1E-12,
    'delta': 1E-12,
}

with open(output+result_file, "w") as f:
    f.close()

for r in r_list:
    print("Distance r = {}".format(r))

    # Place two flux tubes close to the center
    core.x0 = 0.5
    core.y0 = 0.5 + r / 2

    core.x1 = 0.5
    core.y1 = 0.5 - r / 2

    """
        Start of computation
    """

    core.winding = p['winding']
    core.float_type = p['float_type']
    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])
    phi = 4 * np.pi / p['q']
    lr = np.sqrt(phi / B / core.lys)
    ny = [core.nny(n) for n in p['nx']]

    t0 = time.time()

    """
        Condensates
    """

    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])

    nx = p['nx'][-1]
    fac = lr / nx
    rho_sc = np.sqrt((p['mu'][0] ** 2 - p['m'][0] ** 2) / p['la'][0]) * fac
    rho_sf = np.sqrt((p['mu'][1] ** 2 - p['m'][1] ** 2) / p['la'][1]) * fac
    rho_c_1 = np.sqrt(p['la'][1] * (p['la'][0] * rho_sc ** 2 + p['h'] * rho_sf ** 2) / (p['la'][0] * p['la'][1] - p['h'] ** 2))
    rho_c_2 = np.sqrt(p['la'][0] * (p['la'][1] * rho_sf ** 2 + p['h'] * rho_sc ** 2) / (p['la'][0] * p['la'][1] - p['h'] ** 2))

    r01 = rho_sc / fac
    r02 = rho_sf / fac
    la1 = p['la'][0]
    la2 = p['la'][1]
    h = p['h']
    g_coe = - la1 * la2 * (la1 * r01 ** 4 + la2 * r02 ** 4 + 2 * h * r01 ** 2 * r02 ** 2) / (4 * la1 * la2 - 4 * h ** 2)

    # override print statements (i.e. shut up!!!!)
    def _print(a):
        pass
    #core.prnt = _print

    mg = core.MultigridMinimizer(p['nx'])
    results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                                lr, p['epsilon'], p['delta'], p['iters_per_step'], p['total_steps'],
                                0, xa1=None)


    print("Gradient descent took me {:d}s.".format(int(time.time() - t0)))
    f_flux = results[-3]
    h_ext = results[-2]
    b_avg = results[-1]
    g_flux = f_flux - h_ext * b_avg / (4 * np.pi)

    print("F = {}".format(f_flux))
    print("H_ext = {}".format(h_ext))
    print("B_avg = {}".format(b_avg))
    print("G = {}".format(g_flux))

    fmt = "{:.16E} "
    data = [r, r * lr, f_flux, g_coe, f_flux - g_coe, h_ext, b_avg, g_flux, lr ** 2 * core.lys]
    fmt_string = ""
    for d in data:
        fmt_string += fmt.format(d)
    fmt_string += "\n"
    with open(output+result_file, "a") as f:
        f.write(fmt_string)

    if use_display:
        nx = p['nx'][-1]
        ny = core.nny(nx)
        rho1 = x[0::4].reshape((ny, nx)).T
        import matplotlib.pyplot as plt
        plt.clf()
        plt.imshow(rho1 / rho_c_1)
        plt.axes().set_aspect('equal', 'datalim')
        plt.show()


    continue

    nx = p['nx'][-1]
    ny = core.nny(nx)
    rho1 = x[0::4].reshape((ny, nx)).T
    rho2 = x[1::4].reshape((ny, nx)).T
    cx = x[2::4].reshape((ny, nx)).T
    cy = x[3::4].reshape((ny, nx)).T
    mag = b.reshape((ny, nx)).T

    def convert_array_to_mathematica_table(a, factor):
        _nx = a.shape[0]
        _ny = a.shape[1]

        _output = ""
        for _y in range(_ny):
            for _x in range(_nx):
                _xx = _x * lr / _nx
                _yy = _y * lr / _ny * core.lys

                _output += "{0} {1} {2} \n".format(_xx, _yy, a[_x, _y] * factor)

        return _output


    lattice_spacing = lr / nx
    q = p['q']

    if use_profile_output:
        with open(output + "_rho1.dat", "w+") as file:
            file.write(convert_array_to_mathematica_table(rho1, 1.0 / lattice_spacing))
            print("Writing to " + file.name)

        with open(output + "_rho2.dat", "w+") as file:
            file.write(convert_array_to_mathematica_table(rho2, 1.0 / lattice_spacing))
            print("Writing to " + file.name)

        with open(output + "_cx.dat", "w+") as file:
            file.write(convert_array_to_mathematica_table(cx, 1.0 / (lattice_spacing * q)))
            print("Writing to " + file.name)

        with open(output + "_cy.dat", "w+") as file:
            file.write(convert_array_to_mathematica_table(cy, 1.0 / (lattice_spacing * q)))
            print("Writing to " + file.name)

        with open(output + "_b.dat", "w+") as file:
            file.write(convert_array_to_mathematica_table(mag, 1.0 / (lattice_spacing ** 2 * q)))
            print("Writing to " + file.name)

        print("All files written!")