"""
    A script for exporting flux tube lattices for a given set of
    physical parameters.
"""

import os
import time
import numpy as np
import minaun_cl.core as core


"""
        Destination for output files
    
    Example: setting this to "/home/user/fluxtubes" will create five files:
    
    /home/user/fluxtubes_rho1.dat    ... proton condensate 
    /home/user/fluxtubes_rho2.dat    ... neutron condensate
    /home/user/fluxtubes_cx.dat      ... x component of 'supervelocity'
    /home/user/fluxtubes_cy.dat      ... y component of 'supervelocity'
    /home/user/fluxtubes_b.dat       ... magnetic field
    
    The output files are in the format of Mathematica tables.
    
    Example:
    { {x1, y1, f(x1, y1)}, {x2, y2, f(x2, y2)}, {x3, y3, f(x3, y3)}, ... }
"""

output = "/home/dmueller/fluxtubes"

"""
        Magnetic field
    
    This parameter sets the average magnetic field of the flux tube lattice.
    It is related to the smaller length of the rectangular simulation area
    via the total magnetic flux.
    
    Assuming B is the average magnetic field, Phi is the magnetic flux
    and L_x * L_y is the area of the rectangular cell, the length L_x
    is given by
    
    L_x = sqrt( Phi / B / sqrt(3) ).
    
    Note that L_y = sqrt(3) * L_x and the magnetic flux is given by
    
    Phi = 4 * pi / q,
    
    where q is the proton charge.
"""

B = 0.1

# parameter object
p = {
    # physical parameters
    'm': [1.0, 1.0],
    'mu': [1.5, 1.8],
    'la_s': [0.25, 1.20],
    'la_e': [0.10, 0.10],
    'alpha': 0.360,
    'q': 0.17,
    'h': -0.1,
    'winding': 1,

    # numerical parameters
    'float_type': 'double',
    'iters_per_step': 1000,
    'total_steps': 500,
    'nx': [128, 256],
    'pa': [0.19, 0.19, 0.09, 0.09],
    'pb': [0.40, 0.40, 0.60, 0.60],
    'epsilon': 1E-10,
    'delta': 1E-10,
}

"""
    Start of computation
"""

core.winding = p['winding']
core.float_type = p['float_type']
p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])
phi = 4 * np.pi / p['q']
lr = np.sqrt(phi / B / np.sqrt(3))
ny = [core.nny(n) for n in p['nx']]

t0 = time.time()


def _print(a):
    pass


# override print statements (i.e. shut up!!!!)
#core.prnt = _print
mg = core.MultigridMinimizer(p['nx'])
results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                            lr, p['epsilon'], p['delta'], p['iters_per_step'], p['total_steps'],
                            0, xa1=None)

print("Gradient descent took me {:d}s.".format(int(time.time() - t0)))
f_flux = results[-3]
h_ext = results[-2]
b_avg = results[-1]
g_flux = f_flux - h_ext * b_avg / (4 * np.pi)

print("F = {}".format(f_flux))
print("H_ext = {}".format(h_ext))
print("B_avg = {}".format(b_avg))
print("G = {}".format(g_flux))

nx = p['nx'][-1]
ny = core.nny(nx)
rho1 = x[0::4].reshape((ny, nx)).T
rho2 = x[1::4].reshape((ny, nx)).T
cx = x[2::4].reshape((ny, nx)).T
cy = x[3::4].reshape((ny, nx)).T
mag = b.reshape((ny, nx)).T


def convert_array_to_mathematica_table(a, factor):
    _nx = a.shape[0]
    _ny = a.shape[1]

    _output = ""
    for _y in range(_ny):
        for _x in range(_nx):
            _xx = _x * lr / _nx
            _yy = _y * lr / _ny * np.sqrt(3.0)

            _output += "{0} {1} {2} \n".format(_xx, _yy, a[_x, _y] * factor)

    return _output


lattice_spacing = lr / nx
q = p['q']

with open(output + "_rho1.dat", "w+") as file:
    file.write(convert_array_to_mathematica_table(rho1, 1.0 / lattice_spacing))
    print("Writing to " + file.name)

with open(output + "_rho2.dat", "w+") as file:
    file.write(convert_array_to_mathematica_table(rho2, 1.0 / lattice_spacing))
    print("Writing to " + file.name)

with open(output + "_cx.dat", "w+") as file:
    file.write(convert_array_to_mathematica_table(cx, 1.0 / (lattice_spacing * q)))
    print("Writing to " + file.name)

with open(output + "_cy.dat", "w+") as file:
    file.write(convert_array_to_mathematica_table(cy, 1.0 / (lattice_spacing * q)))
    print("Writing to " + file.name)

with open(output + "_b.dat", "w+") as file:
    file.write(convert_array_to_mathematica_table(mag, 1.0 / (lattice_spacing ** 2 * q)))
    print("Writing to " + file.name)

print("All files written!")
