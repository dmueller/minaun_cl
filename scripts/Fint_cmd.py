"""
    A script for studying the pair interaction of two flux tubes
    to compute the interaction potential F_int.

    Some parameters are set via command line arguments for
    computing stuff in batches.
"""

import os
import time, sys
import numpy as np
import minaun_cl.core as core

"""
    Arguments (7):
    
    filename    ...     output file
    alpha       ...     value for alpha which sets \lambda_1 and \lambda_2
    N           ...     grid size
    L           ...     box length
    
    r0          ...     starting distance
    r1          ...     stopping distance
    n_r         ...     number of samples to use in this interval for r
"""

args = sys.argv

if len(args) != 1+7:
    print("Wrong number of arguments!")
    exit()


filename = str(args[1])

alpha = float(args[2])
N = int(args[3])
L = float(args[4])

r0 = float(args[5])
r1 = float(args[6])
r_n = int(args[7])

# Set simulation area to 1:2 rectangle
core.lys = 2.0

# List of distances to compute (include max. distance for flux tube self energy)
r_list = list(np.linspace(r0, r1, r_n+1)) + [L/2.0*core.lys]

# parameter object
p = {
    # physical parameters
    'm': [1.0, 1.0],
    'mu': [1.5, 1.8],
    'la_s': [0.25, 1.20],
    'la_e': [0.10, 0.10],
    'alpha': alpha,
    'q': 0.17,
    'h': -0.1,
    'winding': 1,

    # numerical parameters
    'float_type': 'double',
    'model': 'o4',
    'iters_per_step': 1000,
    'total_steps': 500,
    'nx': [N],
    'pa': [0.33, 0.33, 0.13, 0.13],
    'pb': [0.98, 0.98, 0.98, 0.98],
    #'pa': [0.1, 0.1, 0.05, 0.05],
    #'pb': [0.7, 0.7, 0.7, 0.7],
    'epsilon': 1E-13,
    'delta': 1E-13,
}


# Average magnetic field
B = (4 * np.pi / p['q']) / L ** 2

# Clear file
with open(filename, "w") as f:
    f.close()

# Start loop
for r in r_list:
    print("Distance r = {}".format(r))

    # Place two flux tubes close to the center
    rr = r / (L * core.lys)

    core.x0 = 0.5
    core.y0 = 0.5 + rr / 2

    core.x1 = 0.5
    core.y1 = 0.5 - rr / 2

    """
        Start of computation
    """

    core.winding = p['winding']
    core.float_type = p['float_type']
    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])
    phi = 4 * np.pi / p['q']
    lr = L
    ny = [core.nny(n) for n in p['nx']]

    p['la'] = list(np.array(p['la_s']) + (np.array(p['la_e']) - np.array(p['la_s'])) * p['alpha'])

    t0 = time.time()

    # condensates
    nx = p['nx'][-1]
    fac = lr / nx
    rho_sc = np.sqrt((p['mu'][0] ** 2 - p['m'][0] ** 2) / p['la'][0]) * fac
    rho_sf = np.sqrt((p['mu'][1] ** 2 - p['m'][1] ** 2) / p['la'][1]) * fac
    rho_c_1 = np.sqrt(p['la'][1] * (p['la'][0] * rho_sc ** 2 + p['h'] * rho_sf ** 2) / (p['la'][0] * p['la'][1] - p['h'] ** 2))
    rho_c_2 = np.sqrt(p['la'][0] * (p['la'][1] * rho_sf ** 2 + p['h'] * rho_sc ** 2) / (p['la'][0] * p['la'][1] - p['h'] ** 2))

    r01 = rho_sc / fac
    r02 = rho_sf / fac
    la1 = p['la'][0]
    la2 = p['la'][1]
    h = p['h']
    g_coe = - la1 * la2 * (la1 * r01 ** 4 + la2 * r02 ** 4 + 2 * h * r01 ** 2 * r02 ** 2) / (4 * la1 * la2 - 4 * h ** 2)

    # override print statements (i.e. shut up!!!!)
    def _print(a):
        pass
    core.prnt = _print

    # computation
    mg = core.MultigridMinimizer(p['nx'])
    results, x, b = mg.minimize(p['m'], p['mu'], p['la'], p['q'], p['h'], p['pa'], p['pb'],
                                lr, p['epsilon'], p['delta'], p['iters_per_step'], p['total_steps'],
                                0, xa1=None)
    f_flux = results[-3]
    h_ext = results[-2]
    b_avg = results[-1]
    g_flux = f_flux - h_ext * b_avg / (4 * np.pi)

    # terminal output
    print("Gradient descent took me {:d}s.".format(int(time.time() - t0)))

    print("F = {}".format(f_flux))
    print("H_ext = {}".format(h_ext))
    print("B_avg = {}".format(b_avg))
    print("G = {}".format(g_flux))

    # output
    fmt = "{:.16E} "
    data = [r, f_flux, g_coe, h_ext, b_avg, g_flux, lr ** 2 * core.lys]
    fmt_string = ""
    for d in data:
        fmt_string += fmt.format(d)
    fmt_string += "\n"
    with open(filename, "a") as f:
        f.write(fmt_string)
